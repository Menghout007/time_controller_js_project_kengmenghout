tailwind.config = {
  theme: {
    extend: {
      colors: {
        'colorone': '#82AAE3',
        'colortwo': '#91D8E4', 
        'color2': '#BFEAF5',
        'color3': '#EAFDFC',
        'color4': '#FFF6BD',
        'color5': '#CEEDC7',
        'color6': '#86C8BC',
      },
      backgroundImage: {
        'hero-pattern': "url('https://gifs.eco.br/wp-content/uploads/2022/06/gifs-de-anime-lofi-9.gif')",
      },
      fontFamily: {
        playfair: ['Playfair Display'], 
      }
    }
  }
}
