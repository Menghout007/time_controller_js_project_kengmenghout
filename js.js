let myVar = setInterval(function () { myTimer() }, 1000);
function myTimer() {
    var d = new Date();
    var t = d.toDateString() + " " + d.toLocaleTimeString();
    document.getElementById("demo").innerHTML = t;
}
    var start=true;
    var stop=false;
    var clear=false;
    var countStop;

    function StartFunc(){
        if(start){
        var startAt = new Date();
        var s = startAt.toLocaleTimeString();
        document.getElementById("startAt").innerHTML = s;
        document.getElementById('btn').innerHTML='Stop';
        start = false;
        stop = true;

    }else if(stop){
        var stopAt = new Date();
        var s = stopAt.toLocaleTimeString();
        document.getElementById("stopAt").innerHTML = s;
        document.getElementById('btn').innerHTML='Clear';
        var minutesGet = new Date();
        var Minutes = minutesGet.getSeconds();
        document.getElementById("minutesGet").innerHTML = Minutes;
        stop = false;
        clear = true;
        setTimeout(()=>{
            clearInterval(countStop);
        },1);
        
    } else {
        document.getElementById('btn').innerHTML='Start';
        document.getElementById("startAt").innerHTML='0:00';
        document.getElementById("stopAt").innerHTML='0:00';
        document.getElementById("minutesGet").innerHTML='0:00';
        clear = false;
        start = true;
    }
}